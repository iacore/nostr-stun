// const KEY_PATH: &str = "data/identity";

// fn get_key() -> Result<Keypair, eyre::Report> {
//     match std::fs::read(KEY_PATH) {
//         Ok(key_data) => Ok(Keypair::from_protobuf_encoding(&key_data)?),
//         Err(err) => match err.kind() {
//             std::io::ErrorKind::NotFound => {
//                 let key = Keypair::generate_ed25519();
//                 let a = key.to_protobuf_encoding()?;
//                 std::fs::write(KEY_PATH, &a)?;
//                 Ok(key)
//             }
//             _ => {
//                 return Err(err.into());
//             }
//         },
//     }
// }

fn get_stun() -> Result<(), eyre::Report> {
    // let key = get_key()?;
    use std::net::UdpSocket;
    use std::net::{SocketAddr, ToSocketAddrs};
    use stunclient::StunClient;
    let local_addr: SocketAddr = "0.0.0.0:12327".parse().unwrap();
    let stun_addr = "stun.l.google.com:19302"
        .to_socket_addrs()
        .unwrap()
        .filter(|x| x.is_ipv4())
        .next()
        .unwrap();
    let udp = UdpSocket::bind(local_addr).unwrap();

    let c = StunClient::new(stun_addr);
    match c.query_external_address(&udp) {
        Ok(my_external_addr) => {
            println!("{}", my_external_addr);
        }
        Err(err) => {
            eprintln!("{}", err);
        }
    }
    Ok(())
}