use std::time::Duration;

use nostr_sdk::nostr;

use lazy_static::lazy_static;
use nostr::prelude::*;
use nostr_sdk::Client;
use tracing::{info, instrument, metadata::LevelFilter, trace, trace_span, Instrument};
use tracing_subscriber::prelude::*;

const MESSAGE_KIND: nostr::Kind = nostr::Kind::Ephemeral(27502);
const HASHTAG: &str = "test data (application specific)";
/// the default retention duration of emphemeral events, but NIP11 is needed to get retention duration
const ADVERTISE_INTERVAL: Duration = Duration::from_secs(1);

lazy_static! {
    static ref TAG_DIFF: Tag = Tag::Description(String::from(HASHTAG));
    static ref MESSAGE_FILTER: Filter = Filter {
        kinds: Some(vec![MESSAGE_KIND]),
        hashtags: Some(vec![HASHTAG.to_owned()]),
        ..Default::default()
    };
}

#[instrument(skip_all)]
async fn advertise_forever(client: Client, my_keys: Keys) -> Result<(), eyre::Report> {
    loop {
        let event: Event = EventBuilder::new(
            MESSAGE_KIND,
            "Hello from Nostr SDK",
            &[Tag::Hashtag(HASHTAG.to_owned())],
        )
        .to_event(&my_keys)?;

        let msg = ClientMessage::new_event(event);
        client.send_msg(msg).await?;

        tokio::time::sleep(ADVERTISE_INTERVAL).await;
    }
}

#[instrument(skip_all)]
async fn listen_to_subscription(
    mut rx: tokio::sync::broadcast::Receiver<nostr_sdk::RelayPoolNotification>,
    my_keys: Keys,
) -> Result<(), color_eyre::Report> {
    loop {
        let msg = rx.recv().await?;

        let event = match msg {
            nostr_sdk::RelayPoolNotification::Event(_, event) => event,
            nostr_sdk::RelayPoolNotification::Message(_, msg) => {
                trace!("ignored Message: {:?}", msg);
                continue;
            }
            nostr_sdk::RelayPoolNotification::Shutdown => break,
        };
        if event.pubkey == my_keys.public_key() {
            continue;
        }
        println!("{:?}", event);
    }
    Ok(())
}

#[instrument(skip_all)]
async fn run_chat(client: Client, my_keys: Keys) -> Result<(), eyre::ErrReport> {
    let rx = client.notifications();

    let fut = futures_lite::future::race::<Result<(), eyre::ErrReport>, _, _>(
        advertise_forever(client.clone(), my_keys.clone()),
        listen_to_subscription(rx, my_keys),
    );

    client
        .subscribe(vec![MESSAGE_FILTER.clone()])
        .instrument(trace_span!("#subcribe"))
        .await;

    fut.await
}

#[tokio::main]
async fn main() -> Result<(), eyre::Report> {
    color_eyre::install()?;

    tracing_subscriber::registry()
        .with(tracing_subscriber::fmt::layer())
        .with(
            tracing_subscriber::EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        .with(tracing_error::ErrorLayer::default())
        .init();

    // let args: Vec<_> = args().collect();

    let my_keys = Keys::generate();

    // let bech32_pubkey: String = my_keys.public_key().to_bech32()?;
    // println!("Bech32 PubKey: {}", bech32_pubkey);

    // Connect to relay
    let client = Client::new(&my_keys);

    // Add relays
    for s in ["wss://relay.damus.io"] {
        let url: Url = s.parse()?;
        client.add_relay(s, None).await?;
        client.connect_relay(s).await?;
        let relay = client.relay(&url).await.expect("Relay already added");
        let doc = relay.document().await;
        info!("relay info: url={} {:?}", s, doc);
        if !support_nips(doc, &[]) {
            client.disconnect_relay(s).await?;
            client.remove_relay(s).await?;
            info!("skipped relay due to lack of features: {}", s);
        } else {
            info!("added relay: {}", s);
        }
        // doc.supported_nips
        // supported_nips
    }
    client.connect().await;
    info!("connected to all relays");

    run_chat(client, my_keys).await
}

fn support_nips(doc: RelayInformationDocument, nips: &[u16]) -> bool {
    match doc.supported_nips {
        Some(supported) => {
            for nip in nips {
                if !supported.contains(nip) {
                    return false;
                }
            }
            true
        }
        None => return nips.len() == 0,
    }
}
